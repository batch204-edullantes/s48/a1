
fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => {
	console.log(response);
	return response.json();
}).then(data => {
	showPosts(data);
	console.log(data);
});

const showPosts = (posts) => {
	let postEntries = '';

	// loop through each post in our posts array
	posts.forEach((post) => {
		postEntries += `
			<div id="post-${post.id}">
				<h3 id="post-title-${post.id}">${post.title}</h3>
				<p id="post-body-${post.id}">${post.body}</p>
				<button onClick="editPost(${post.id})" id="edit-btn-${post.id}">Edit</button>
				<button onClick="deletePost(${post.id})" id="delete-btn-${post.id}">Delete</button>
			</div>
		`
	})

	// insert the postEntries HTML code into the empty div in our HTML
	document.querySelector('#div-post-entries').innerHTML = postEntries;
}



const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML
	let body = document.querySelector(`#post-body-${id}`).innerHTML

	//populate the edit form fields
	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;
}

const deletePost = (id) => {
	document.querySelector(`#post-title-${id}`).innerHTML = '';
	document.querySelector(`#post-body-${id}`).innerHTML = '';
	document.querySelector(`#edit-btn-${id}`).style.display = 'none';
	document.querySelector(`#delete-btn-${id}`).style.display = 'none';
}
